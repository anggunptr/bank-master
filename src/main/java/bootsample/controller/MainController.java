package bootsample.controller;

import javax.servlet.http.HttpServletRequest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController {

//    @Autowired
//    private TaskService taskService;
//    @ApiOperation(value = "Get Data 123",
//    notes = "returns data")
    @GetMapping("/home")
    public String home(HttpServletRequest request) {
	request.setAttribute("mode", "MODE_HOME");
        return "transaksi";
    }

}
