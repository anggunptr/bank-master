package bootsample.controller;

import bootsample.model.Deposit;
import bootsample.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TransaksiController {

    @GetMapping("/deposit")
    public String Deposit(HttpServletRequest request){
        request.setAttribute("mode", "MODE_DEPOSIT");
        return "transaksi";
    }
    @GetMapping("/withdraw")
    public String Withdraw(HttpServletRequest request){
        request.setAttribute("mode", "MODE_WITHDRAW");
        return "transaksi";
    }
    @GetMapping("/transfer")
    public String Transfer(HttpServletRequest request){
        request.setAttribute("mode", "MODE_TRANSFER");
        return "transaksi";
    }

    @Autowired
    private DepositService depositService;
    @PostMapping("/save-deposit")
    public String saveDeposit(@ModelAttribute Deposit deposit, BindingResult bindingResult, HttpServletRequest request){
        depositService.save(deposit);
        request.setAttribute("deposits", depositService.findAll());
        request.setAttribute("mode", "MODE_DEPOSIT");
        return "transaksi";
    }
}
