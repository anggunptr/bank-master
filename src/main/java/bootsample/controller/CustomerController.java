package bootsample.controller;

import bootsample.model.Account;
import bootsample.model.Nasabah;
import bootsample.service.AccountService;
import bootsample.service.NasabahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomerController {

    @GetMapping("/customer-list")
    public String CustomerList(HttpServletRequest request){
        request.setAttribute("nasabahs", nasabahService.findAll());
        request.setAttribute("mode", "MODE_CUSTOMER");
        return "customer";
    }
    @GetMapping("/input-customer")
    public String nasabah(HttpServletRequest request){
//        request.setAttribute("tasks", taskService.findAll());
        request.setAttribute("mode", "MODE_NASABAH");
        return "customer";
    }
    @GetMapping("/input-account")
    public String account(HttpServletRequest request){
//        request.setAttribute("tasks", taskService.findAll());
        request.setAttribute("mode", "MODE_ACCOUNT");
        return "customer";
    }
    @Autowired
    private NasabahService nasabahService;
    @PostMapping("/save-nasabah")
    public String saveNasabah(@ModelAttribute Nasabah nasabah, BindingResult bindingResult, HttpServletRequest request){
        nasabahService.save(nasabah);
        request.setAttribute("mode", "MODE_NASABAH");
        return "customer";
    }

    @GetMapping("/update-nasabah")
    public String updateNasabah(@RequestParam int idnasabah, HttpServletRequest request){
        request.setAttribute("nasabah", nasabahService.findNasabah(idnasabah));
        request.setAttribute("mode", "MODE_UPDATE");
        return "customer";
    }

    @GetMapping("/delete-nasabah")
    public String deleteNasabah(@RequestParam int idnasabah, HttpServletRequest request){
        nasabahService.delete(idnasabah);
        request.setAttribute("nasabahs", nasabahService.findAll());
        request.setAttribute("mode", "MODE_CUSTOMER");
        return "customer";
    }
    @Autowired
    private AccountService accountService;
    @PostMapping("/save-account")
    public String saveAccount(@ModelAttribute Account account, BindingResult bindingResult, HttpServletRequest request){
        accountService.save(account);
        request.setAttribute("mode", "MODE_ACCOUNT");
        return "customer";
    }


}
