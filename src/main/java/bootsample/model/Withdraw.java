package bootsample.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="trx_withdraw")
public class Withdraw implements Serializable {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private int idtrxwithdraw;
    private int norek;
    private int jmlwithdraw;
    @Temporal(TemporalType.TIMESTAMP)
    private Date tglwithdraw = new Date();

    public Withdraw(){

    }

    public Withdraw(int norek, int jmlwithdraw, Date tglwithdraw) {
        this.norek = norek;
        this.jmlwithdraw = jmlwithdraw;
        this.tglwithdraw = tglwithdraw;
    }

    public int getIdtrxwithdraw() {
        return idtrxwithdraw;
    }

    public void setIdtrxwithdraw(int idtrxwithdraw) {
        this.idtrxwithdraw = idtrxwithdraw;
    }

    public int getNorek() {
        return norek;
    }

    public void setNorek(int norek) {
        this.norek = norek;
    }

    public int getJmlwithdraw() {
        return jmlwithdraw;
    }

    public void setJmlwithdraw(int jmlwithdraw) {
        this.jmlwithdraw = jmlwithdraw;
    }

    public Date getTglwithdraw() {
        return tglwithdraw;
    }

    public void setTglwithdraw(Date tglwithdraw) {
        this.tglwithdraw = tglwithdraw;
    }

    @Override
    public String toString() {
        return "Withdraw{" +
                "idtrxwithdraw=" + idtrxwithdraw +
                ", norek=" + norek +
                ", jmlwithdraw=" + jmlwithdraw +
                ", tglwithdraw=" + tglwithdraw +
                '}';
    }
}
