package bootsample.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "md_account")
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int norek;
    private int idnasabah;
    private int saldo;

    public Account() {
    }

    public Account(int idnasabah, int saldo) {
        super();
        this.idnasabah = idnasabah;
        this.saldo = saldo;
    }

    public int getNorek() {
        return norek;
    }

    public void setNorek(int norek) {
        this.norek = norek;
    }

    public int getIdnasabah() {
        return idnasabah;
    }

    public void setIdnasabah(int idnasabah) {
        this.idnasabah = idnasabah;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Account{" +
                "norek=" + norek +
                ", idnasabah=" + idnasabah +
                ", saldo=" + saldo +
                '}';
    }
}
