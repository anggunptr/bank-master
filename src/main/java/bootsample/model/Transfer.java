package bootsample.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "trx_transfer")
public class Transfer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idtrxtransfer;
    private int norekpengirim;
    private int norekpenerima;
    private int jmltransfer;
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgltransfer = new Date();

    public Transfer() {
    }

    public Transfer(int norekpengirim, int norekpenerima, int jmltransfer, Date tgltransfer) {
        this.norekpengirim = norekpengirim;
        this.norekpenerima = norekpenerima;
        this.jmltransfer = jmltransfer;
        this.tgltransfer = tgltransfer;
    }

    public int getIdtrxtransfer() {
        return idtrxtransfer;
    }

    public void setIdtrxtransfer(int idtrxtransfer) {
        this.idtrxtransfer = idtrxtransfer;
    }

    public int getNorekpengirim() {
        return norekpengirim;
    }

    public void setNorekpengirim(int norekpengirim) {
        this.norekpengirim = norekpengirim;
    }

    public int getNorekpenerima() {
        return norekpenerima;
    }

    public void setNorekpenerima(int norekpenerima) {
        this.norekpenerima = norekpenerima;
    }

    public int getJmltransfer() {
        return jmltransfer;
    }

    public void setJmltransfer(int jmltransfer) {
        this.jmltransfer = jmltransfer;
    }

    public Date getTgltransfer() {
        return tgltransfer;
    }

    public void setTgltransfer(Date tgltransfer) {
        this.tgltransfer = tgltransfer;
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "idtrxtransfer=" + idtrxtransfer +
                ", norekpengirim=" + norekpengirim +
                ", norekpenerima=" + norekpenerima +
                ", jmltransfer=" + jmltransfer +
                ", tgltransfer=" + tgltransfer +
                '}';
    }
}