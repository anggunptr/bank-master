package bootsample.model;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "md_nasabah")
public class Nasabah implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idnasabah;
    private String namanasabah;
    private String noktp;
    private String alamat;
    private String tmptlahir;
    private String tgllahir;
    private String namaibu;
    private String jeniskelamin;

    public Nasabah(){

    }
    public Nasabah(String namanasabah, String noktp, String alamat, String tmptlahir, String tgllahir, String namaibu, String jeniskelamin) throws ParseException {
        super();
        this.namanasabah = namanasabah;
        this.noktp = noktp;
        this.alamat = alamat;
        this.tmptlahir = tmptlahir;
        this.tgllahir = tgllahir;
        this.namaibu = namaibu;
        this.jeniskelamin = jeniskelamin;
    }

    public int getIdnasabah() {
        return idnasabah;
    }

    public void setIdnasabah(int idnasabah) {
        this.idnasabah = idnasabah;
    }

    public String getNamanasabah() {
        return namanasabah;
    }

    public void setNamanasabah(String namanasabah) {
        this.namanasabah = namanasabah;
    }

    public String getNoktp() {
        return noktp;
    }

    public void setNoktp(String noktp) {
        this.noktp = noktp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTmptlahir() {
        return tmptlahir;
    }

    public void setTmptlahir(String tmptlahir) {
        this.tmptlahir = tmptlahir;
    }

    public String getTgllahir() {
        return tgllahir;
    }

    public void setTgllahir(String tgllahir) {
        this.tgllahir = tgllahir;
    }

    public String getNamaibu() {
        return namaibu;
    }

    public void setNamaibu(String namaibu) {
        this.namaibu = namaibu;
    }

    public String getJeniskelamin() {
        return jeniskelamin;
    }

    public void setJeniskelamin(String jeniskelamin) {
        this.jeniskelamin = jeniskelamin;
    }

    @Override
    public String toString() {
        return "Nasabah{" +
                "idnasabah=" + idnasabah +
                ", namanasabah='" + namanasabah + '\'' +
                ", noktp='" + noktp + '\'' +
                ", alamat='" + alamat + '\'' +
                ", tmptlahir='" + tmptlahir + '\'' +
                ", tgllahir='" + tgllahir + '\'' +
                ", namaibu='" + namaibu + '\'' +
                ", jeniskelamin='" + jeniskelamin + '\'' +
                '}';
    }
}