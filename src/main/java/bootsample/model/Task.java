package bootsample.model;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity(name = "trx_deposit")
public class Task implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idtrxdeposit;
    private int norek;
    private int jmldeposit;
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgldeposit = new Date();

    public Task() {

    }

    public Task(int norek, int jmldeposit, Date tgldeposit) {
        super();
        this.norek = norek;
        this.jmldeposit = jmldeposit;
        this.tgldeposit = tgldeposit;
    }

    public int getIdtrxdeposit() {
        return idtrxdeposit;
    }

    public void setIdtrxdeposit(int idtrxdeposit) {
        this.idtrxdeposit = idtrxdeposit;
    }

    public int getNorek() {
        return norek;
    }

    public void setNorek(int norek) {
        this.norek = norek;
    }

    public int getJmldeposit() {
        return jmldeposit;
    }

    public void setJmldeposit(int jmldeposit) {
        this.jmldeposit = jmldeposit;
    }

    public Date getTgldeposit() {
        return tgldeposit;
    }

    public void setTgldeposit(Date tgldeposit) {
        this.tgldeposit = tgldeposit;
    }

    @Override
    public String toString() {
        return "Task{" +
                "idtrxdeposit=" + idtrxdeposit +
                ", norek=" + norek +
                ", jmldeposit=" + jmldeposit +
                ", tgldeposit=" + tgldeposit +
                '}';
    }
}
