package bootsample.dao;

import bootsample.model.Nasabah;
import org.springframework.data.repository.CrudRepository;

public interface NasabahRepository extends CrudRepository<Nasabah, Integer> {
}
