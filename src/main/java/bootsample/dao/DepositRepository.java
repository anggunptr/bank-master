package bootsample.dao;

import bootsample.model.Deposit;
import org.springframework.data.repository.CrudRepository;

public interface DepositRepository extends CrudRepository<Deposit, Integer> {
}
