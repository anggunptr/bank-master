package bootsample.dao;

import bootsample.model.Nasabah;
import bootsample.model.Transfer;
import org.springframework.data.repository.CrudRepository;

public interface TransferRepository extends CrudRepository<Transfer, Integer> {
}
