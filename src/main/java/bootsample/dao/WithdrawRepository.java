package bootsample.dao;

import bootsample.model.Withdraw;
import org.springframework.data.repository.CrudRepository;

public interface WithdrawRepository extends CrudRepository<Withdraw, Integer> {
}