package bootsample.service;
import bootsample.dao.TransferRepository;
import bootsample.model.Transfer;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional

public class TransferService {


    private final TransferRepository transferRepository;

    public TransferService(TransferRepository transferRepository) {
        this.transferRepository = transferRepository;
    }

    public List<Transfer> findAll() {
        List<Transfer> transfers = new ArrayList<>();
        for (Transfer transfer : transferRepository.findAll()) {
            transfers.add(transfer);
        }
        return transfers;
    }

    public Transfer findTask(int idtrxtransfer) {
        return transferRepository.findOne(idtrxtransfer);
    }

    public void save(Transfer transfer) {
        transferRepository.save(transfer);
    }


    public void delete(int idtrxtransfer) {
        transferRepository.delete(idtrxtransfer);
    }
}