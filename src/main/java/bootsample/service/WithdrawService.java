package bootsample.service;

import bootsample.dao.WithdrawRepository;
import bootsample.model.Withdraw;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class WithdrawService {


    private final WithdrawRepository withdrawRepository;

    public WithdrawService(WithdrawRepository withdrawRepository) {
        this.withdrawRepository = withdrawRepository;
    }

    public List<Withdraw> findAll() {
        List<Withdraw> withdraws = new ArrayList<>();
        for (Withdraw withdraw : withdrawRepository.findAll()) {
            withdraws.add(withdraw);
        }
        return withdraws;
    }

    public Withdraw findTask(int idtrxwithdraw) {
        return withdrawRepository.findOne(idtrxwithdraw);
    }

    public void save(Withdraw withdraw) {
        withdrawRepository.save(withdraw);
    }


    public void delete(int idtrxwithdraw) {
        withdrawRepository.delete(idtrxwithdraw);
    }
}
