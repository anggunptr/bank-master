package bootsample.service;

import bootsample.dao.NasabahRepository;
import bootsample.model.Deposit;
import bootsample.model.Nasabah;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class NasabahService {

    private final NasabahRepository nasabahRepository;

    public NasabahService(NasabahRepository nasabahRepository) {
        this.nasabahRepository = nasabahRepository;
    }

    public List<Nasabah>findAll() {
        List<Nasabah> nasabahs= new ArrayList<>();
        for (Nasabah nasabah : nasabahRepository.findAll()) {
            nasabahs.add(nasabah);
        }
        return nasabahs;
    }
    public Nasabah findNasabah (int idnasabah) {
        return nasabahRepository.findOne(idnasabah);
    }
    public void save(Nasabah nasabah) {
        nasabahRepository.save(nasabah);
    }


    public void delete(int idnasabah) {
        nasabahRepository.delete(idnasabah);
    }
}
