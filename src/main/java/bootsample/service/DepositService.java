package bootsample.service;

import bootsample.dao.DepositRepository;
import bootsample.model.Deposit;
import bootsample.model.Task;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DepositService {

    private final DepositRepository depositRepository;

    public DepositService(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    public List<Deposit> findAll() {
        List<Deposit> deposits = new ArrayList<>();
        for (Deposit deposit : depositRepository.findAll()) {
            deposits.add(deposit);
        }
        return deposits;
    }

    public Deposit findDeposit (int idtrxdeposit) {
        return depositRepository.findOne(idtrxdeposit);
    }

    public void save(Deposit deposit) {
        depositRepository.save(deposit);
    }


    public void delete(int idtrxdeposit) {
        depositRepository.delete(idtrxdeposit);
    }
}