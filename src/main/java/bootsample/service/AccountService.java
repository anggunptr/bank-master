package bootsample.service;

import bootsample.dao.AccountRepository;
import bootsample.model.Account;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {

        this.accountRepository = accountRepository;
    }

    public List<Account> showAllNasabahs() {
        List<Account> accounts= new ArrayList<>();
        for (Account account : accountRepository.findAll()) {
            accounts.add(account);
        }
        return accounts;
    }

    public void save(Account account) {
        accountRepository.save(account);
    }


    public void delete(int norek) {
        accountRepository.delete(norek);
    }
}