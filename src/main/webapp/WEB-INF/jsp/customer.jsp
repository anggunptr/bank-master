<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

    <title>Task Manager | Home</title>

    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <%--    <link href="static/css/bootstrap.min.css" rel="stylesheet">--%>
    <%--    <link href="static/css/style.css" rel="stylesheet">--%>

    <%--    <!--[if lt IE 9]>--%>
    <%--    <script src="static/js/html5shiv.min.js"></script>--%>
    <%--    <script src="static/js/respond.min.js"></script>--%>

    <![endif]-->
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-warning static-top">
    <div class="container">
        <a class="navbar-brand" href="home">
            <img src="static/bank/pji.jpeg" width="30" height="30" class="d-inline-block align-top" alt="">
            PJI Bank
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="home">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Transaction
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                        <a class="dropdown-item" href="deposit">Deposit</a>
                        <a class="dropdown-item" href="withdraw">Withdraw</a>
                        <a class="dropdown-item" href="transfer">Transfer</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Report
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Deposit</a>
                        <a class="dropdown-item" href="#">Withdraw</a>
                        <a class="dropdown-item" href="#">Transfer</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Customer
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                        <a class="dropdown-item" href="input-customer">Add Data Customer</a>
                        <a class="dropdown-item" href="input-account">Add Account</a>
                        <a class="dropdown-item" href="customer-list">Customer List</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<c:choose>
    <c:when test="${mode == 'MODE_HOME'}">
        <div class="container" id="homeDiv">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="static\bank\bank1.jpg" class="d-block w-100" alt="..."></a>
                    </div>
                    <div class="carousel-item">
                        <img src= "static\bank\bank2.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
                <%--            <div class="jumbotron text-center">--%>
                <%--                <h1>Welcome to Task Manager</h1>--%>
                <%--            </div>--%>
        </div>
    </c:when>
    <c:when test="${mode == 'MODE_NASABAH'}">
        <form class="form-horizontal" method="POST" action="/save-nasabah">
            <input type="hidden" name="idnasabah" value="${nasabah.idnasabah}"/>
            <div class="form-group">
                <label class="control-label col-md-3">Nama Nasabah</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="namanasabah" value="${nasabah.namanasabah}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">No Ktp</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="noktp" value="${nasabah.noktp}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Alamat</label>
                <div class="col-md-7">
                    <input type="area" class="form-control" name="alamat" value="${nasabah.alamat}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Tempat Lahir</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="tmptlahir" value="${nasabah.tmptlahir}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Tanggal Lahir</label>
                <div class="col-md-7">
                    <input type="date" class="form-control" name="tgllahir" value="${nasabah.tgllahir}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Nama Ibu Kandung</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="namaibu" value="${nasabah.namaibu}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Jenis Kelamin</label>
                <input class="form-check-input" type="radio" name="jeniskelamin" id="jeniskelamin" value="laki-laki" >
                <label class="form-check-label"> Lakilaki
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jeniskelamin" id="jeniskelamin2" value="perempuan">
                <label class="form-check-label">Perempuan
                </label>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Save"/>
            </div>
        </form>
    </c:when>

    <c:when test="${mode == 'MODE_CUSTOMER'}">
        <div class="container text-center" id="tasksDiv">
            <h3>CUSTOMER LIST</h3>
            <hr>
            <div class="table-responsive">
                <table class="table table-striped table-bordered text-left">
                    <thead>
                    <th>
                            <%--                            <th>id nasabah</th>--%>
                    <th>nama nasabah</th>
                    <th>no ktp</th>
                    <th>alamat</th>
                    <th>tempat lahir</th>
                    <th>tanggal lahir</th>
                    <th>nama ibu kandung</th>
                    <th>jenis kelamin</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="nasabah" items="${nasabahs}">
                        <tr>
                            <td>${nasabah.idnasabah}</td>
                            <td>${nasabah.namanasabah}</td>
                            <td>${nasabah.noktp}</td>
                            <td>${nasabah.alamat}</td>
                            <td>${nasabah.tmptlahir}</td>
                            <td>${nasabah.tgllahir}</td>
                            <td>${nasabah.namaibu}</td>
                            <td>${nasabah.jeniskelamin}</td>
                            <td><a href="update-task?idnasabah=${nasabah.idnasabah}"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                            <td><a href="delete-task?idnasabah=${nasabah.idnasabah}"><span class="glyphicon glyphicon-trash"></span></a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </c:when>
    <c:when test="${mode == 'MODE_ACCOUNT'}">
        <form class="form-horizontal" method="POST" action="/save-account">
            <input type="hidden" name="norek" value="${account.norek}"/>
            <div class="form-group">
                <label class="control-label col-md-3">Id Nasabah</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="idnasabah" value="${account.idnasabah}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Saldo</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="saldo" value="${account.saldo}"/>
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Save"/>
            </div>
        </form>
    </c:when>
</c:choose>

<%--<script src="static/js/jquery-1.11.1.min.js"></script>--%>
<%--<script src="static/js/bootstrap.min.js"></script>--%>
<script src= "static/jquery/jquery.min.js"></script>
<script src= "static/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>