<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="Sat, 01 Dec 2001 00:00:00 GMT">

    <title>Task Manager | Home</title>

    <link href="static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<%--    <link href="static/css/bootstrap.min.css" rel="stylesheet">--%>
<%--    <link href="static/css/style.css" rel="stylesheet">--%>

<%--    <!--[if lt IE 9]>--%>
<%--    <script src="static/js/html5shiv.min.js"></script>--%>
<%--    <script src="static/js/respond.min.js"></script>--%>

    <![endif]-->
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-warning static-top">
    <div class="container">
        <a class="navbar-brand" href="home">
            <img src="static/bank/pji.jpeg" width="30" height="30" class="d-inline-block align-top" alt="">
            PJI Bank
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="home">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Transaction
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                        <a class="dropdown-item" href="deposit">Deposit</a>
                        <a class="dropdown-item" href="withdraw">Withdraw</a>
                        <a class="dropdown-item" href="transfer">Transfer</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Report
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Deposit</a>
                        <a class="dropdown-item" href="#">Withdraw</a>
                        <a class="dropdown-item" href="#">Transfer</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Customer
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                        <a class="dropdown-item" href="input-customer">Add Data Customer</a>
                        <a class="dropdown-item" href="input-account">Add Account</a>
                        <a class="dropdown-item" href="customer-list">Customer List</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<%--&lt;%&ndash;<div role="navigation">&ndash;%&gt;--%>
<%--    <div class="navbar navbar-inverse">--%>
<%--        <a href="/" class="navbar-brand">PJI Bank</a>--%>
<%--        <div class="navbar-collapse collapse">--%>
<%--            <ul class="nav navbar-nav">--%>
<%--                <li class="nav-item dropdown">--%>
<%--                    <a class="nav-link dropdown-toggle" href="#" id="navbar Dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--%>
<%--                        Transaction--%>
<%--                    </a>--%>
<%--                    <div class="dropdown-menu" aria-labelledby="navbar Dropdown">--%>
<%--                        <a class="dropdown-item" href="deposit">Deposit</a>--%>
<%--                        <a class="dropdown-item" href="withdraw">Withdraw</a>--%>
<%--                        <a class="dropdown-item" href="transfer">Transfer</a>--%>
<%--                    </div>--%>
<%--                </li>--%>
<%--                <li><a href="new-task">New Task</a></li>--%>
<%--                <li><a href="all-tasks">All Tasks</a></li>--%>
<%--                <li><a href="about">About</a></li>--%>
<%--            </ul>--%>
<%--        </div>--%>
<%--    </div>--%>
<%--</div>--%>


<c:choose>
    <c:when test="${mode == 'MODE_HOME'}">
        <div class="container" id="homeDiv">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="static\bank\bank1.jpg" class="d-block w-100" alt="..."></a>
                    </div>
                    <div class="carousel-item">
                        <img src= "static\bank\bank2.jpg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
<%--            <div class="jumbotron text-center">--%>
<%--                <h1>Welcome to Task Manager</h1>--%>
<%--            </div>--%>
        </div>
    </c:when>
    <c:when test="${mode == 'MODE_DEPOSIT'}">
        <form class="form-horizontal" method="POST" action="save-deposit">
            <input type="hidden" name="idtrxdeposit" value="${deposit.idtrxdeposit}"/>
            <div class="form-group">
                <label class="control-label col-md-3">No Rekening</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="norek" value="${deposit.norek}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Jumlah Deposit</label>
                <div class="col-md-7">
                    <input type="text" class="form-control" name="jmldeposit" value="${deposit.jmldeposit}"/>
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Save"/>
            </div>
        </form>
</c:when>
    <c:when test="${mode == 'MODE_WITHDRAW'}">
        <div class="form-group">
            <label>No Rekening</label> <input type="text" class="form-control" id="norek1">
        </div>
        <div class="form-group">
            <label>Jumlah WITHDRAW</label><input type="text" class="form-control" id="jmlwithdraw">
        </div>
        <div> <button type="submit" class="btn btn-primary">Submit</button></div>
    </c:when>
    <c:when test="${mode == 'MODE_TRANSFER'}">
        <div class="form-group">
            <label>No Rekening Pengirim</label> <input type="text" class="form-control" id="rekpengirim">
        </div>
        <div class="form-group">
            <label>No Rekening Penerima</label><input type="text" class="form-control" id="rekpenerima">
        </div>
        <div class="form-group">
            <label>Jumlah Transaksi</label><input type="text" class="form-control" id="jmltrx">
        </div>
        <div> <button type="submit" class="btn btn-primary">Submit</button></div>
    </c:when>
<%--    <c:when test="${mode == 'MODE_TASKS'}">--%>
<%--        <div class="container text-center" id="tasksDiv">--%>
<%--            <h3>My Tasks</h3>--%>
<%--            <hr>--%>
<%--            <div class="table-responsive">--%>
<%--                <table class="table table-striped table-bordered text-left">--%>
<%--                    <thead>--%>
<%--                    <tr>--%>
<%--                        <th>idAccount</th>--%>
<%--                        <th>idCustomer</th>--%>
<%--                        <th>Balance</th>--%>
<%--                        <th></th>--%>
<%--                        <th></th>--%>
<%--                    </tr>--%>
<%--                    </thead>--%>
<%--                    <tbody>--%>
<%--                    <c:forEach var="task" items="${tasks}">--%>
<%--                        <tr>--%>
<%--                            <td>${task.id}</td>--%>
<%--                            <td>${task.idCustomer}</td>--%>
<%--                            <td>${task.balance}</td>--%>
<%--                            <td><a href="update-task?id=${task.id}"><span class="glyphicon glyphicon-pencil"></span></a>--%>
<%--                            </td>--%>
<%--                            <td><a href="delete-task?id=${task.id}"><span class="glyphicon glyphicon-trash"></span></a>--%>
<%--                            </td>--%>
<%--                        </tr>--%>
<%--                    </c:forEach>--%>
<%--                    </tbody>--%>
<%--                </table>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </c:when>--%>
<%--    <c:when test="${mode == 'MODE_NEW' || mode == 'MODE_UPDATE'}">--%>
<%--        <div class="container text-center">--%>
<%--            <h3>Manage Task</h3>--%>
<%--            <hr>--%>
<%--            <form class="form-horizontal" method="POST" action="save-task">--%>
<%--                <input type="hidden" name="id" value="${task.id}"/>--%>
<%--                <div class="form-group">--%>
<%--                    <label class="control-label col-md-3">idCustomer</label>--%>
<%--                    <div class="col-md-7">--%>
<%--                        <input type="text" class="form-control" name="idCustomer" value="${task.idCustomer}"/>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="form-group">--%>
<%--                    <label class="control-label col-md-3">Balance</label>--%>
<%--                    <div class="col-md-7">--%>
<%--                        <input type="text" class="form-control" name="balance" value="${task.balance}"/>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <div class="form-group">--%>
<%--                    <input type="submit" class="btn btn-primary" value="Save"/>--%>
<%--                </div>--%>
<%--            </form>--%>
<%--        </div>--%>
<%--    </c:when>--%>
    <c:when test="${mode == 'MODE_ABOUT'}">
        <div class="container" id="aboutDiv">
            <div class="jumbotron text-center">
                <h1>Welcome to About</h1>
            </div>
        </div>
    </c:when>
</c:choose>

<%--<script src="static/js/jquery-1.11.1.min.js"></script>--%>
<%--<script src="static/js/bootstrap.min.js"></script>--%>
<script src= "static/jquery/jquery.min.js"></script>
<script src= "static/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>